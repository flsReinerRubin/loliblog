package LoliBlog::Model::Post;
{
    use strict;
    use warnings;
    use POSIX qw(strftime);

    my $defOnPage = 10;
    my $postsDir = './blog/pub'; ## TODO make conf

    sub new {
	my ($class, $conf) = @_;

	if ($conf) {
	    $conf->{'blogDir'} && ($postsDir = $conf->{'blogDir'});
	    defined $conf->{'onPage'} && ($defOnPage = $conf->{'onPage'})
	}

 	my $posts = { posts => _getPostsList($postsDir) };

	bless $posts => $class;
    }

    sub _getPostsList {
	my ($dir) = @_;

	my $postsList = [];

	opendir (DIR, $dir) or die $!;

	while (my $postDir = readdir(DIR)) {

	    next unless ($postDir =~ m/^V\d+__.*/); # skip not Vtimestamp
	    my @postContent = glob (join "/" => $dir => $postDir => "*");

	    my $post = {
		'_postDir' => $postDir,
		'id' => ($postDir =~ m/^V(\d+)/),
		'pics' => [],
	    };
	    for my $postFile (@postContent) {
		if(@{$post->{'pics'}} <= 3 && $postFile =~ /.*\.(jpeg|jpg|png|gif)/i) {
		    push @{$post->{'pics'}} => (($postFile =~ /(V\d+__.+$)/) && $1);
		}
		if($postFile =~ /.*\.?pub$/) {
		    my $postText = _getPostText($postFile);

		    next unless $postFile; ## {} is true or no?

		    $post->{'body'} = $postText->{'body'};
		    $post->{'title'} = $postText->{'title'};
		}
	    }
	    push @$postsList, $post;
	}

	closedir(DIR);

	return $postsList;
    }

    sub savePost {
	my ($self, $post) = @_;

	my $postEntity = "";
	my $datestring = strftime "%Y%m%d%H%M%S", localtime;

	{

	    my $postDir = "V" . "${datestring}" . "__yet_another_post_" .  int(rand(100));

	    $postEntity = join "/" => $postsDir => $postDir;
	    mkdir($postEntity);
	}

	open POST, '>', (join "/" => $postEntity => 'pub');

	print POST ($post->{'title'} . "\n");
	print POST ($post->{'body'});

	$_->move_to(join "/" =>  $postEntity => $_->filename) for @{$post->{'uploads'}};

	close POST;

	return $datestring;
    }

    sub getPosts {
	my ($self, $toSkip, $atPage) = @_;
	$toSkip ||= 0;
	$atPage ||= $defOnPage;

	{
	    posts => [grep { $_ } (
			  (sort { $b->{'_postDir'} cmp $a->{'_postDir'} } @{$self->{'posts'}})[$toSkip..$toSkip + $atPage]
		      )],
	    meta => {
		toSkip => $toSkip,
		atPage => $atPage,
		count => scalar(@{$self->{'posts'}}),
	    }
	}
    }

    sub getPost {
	my ($self, $params) = @_;
	# TODO customs params
	{
	    posts => [
		grep
		{ $_->{'id'} eq $params->{'id'}  }
		@{$self->{'posts'}}
		],
	    meta => {
		toSkip => 0,
		atPage => $defOnPage,
		count => $defOnPage,
	    },
	}
    }

    sub _getPostText {
	open(POST, '<:encoding(UTF-8)', shift) || return;

	my ($title, $body) = ("", "");

	while(my $line = <POST>) {
	    unless ($title) {
		$title = $line;
		next;
	    }
	    $body .= $line;
	}

        close(POST);
	return {
	    title => $title,
	    body => $body
	}
    }
}
1;
