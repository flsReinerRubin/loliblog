package LoliBlog::Controller::Posts;
{
    use Mojo::Base 'Mojolicious::Controller';

    sub showPost {
	my $c = shift;
	my $posts = $c->app->posts;

	my $postId = $c->param('postId');

	my $postsList = $posts->getPost({ 'id' =>  $postId });

	$c->render(
	    'index',
	    'title' => 'Welcome. Yay!',
	    'posts' => $postsList,
	    'settings' => { 'showPics' => 'yep' },
	    );
    }

    sub posts {
	my $c = shift;
	my $posts = $c->app->posts;

	my $toSkip = $c->param('toSkip');
	my $postsList = $posts->getPosts($toSkip);

	$c->render(
	    'index',
	    'title' => 'Welcome. Yay!',
	    'posts' => $postsList,
	    'settings' => {},
	    );
    }

    sub postForm {
	my $c = shift;
	$c->render(
	    'posts/postForm',
	    );
    }

    sub addPost {
	my $c = shift;

	my $post = {
	    title =>   $c->req->body_params->param('post-title'),
	    body  =>   $c->req->body_params->param('post-body'),
	    uploads => $c->req->every_upload('post-files[]'),
	};
	my $postId = $c->app->posts->savePost($post);

	$c = $c->redirect_to("/post/show/$postId");
    }

    sub postStaticEntity {
	my $c = shift;
	my $postEntity = $c->param('postEntity');

	my $pathToPostEntity = "/$postEntity";

	$c->app->static->serve($c, $pathToPostEntity);
	$c->stash->{'mojo.static'}++; ## not sure
	$c->rendered;
    }
}
1;
