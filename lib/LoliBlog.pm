package LoliBlog;
{
    use Mojo::Base 'Mojolicious';

    use Mojolicious::Plugin::Config;
    use LoliBlog::Model::Post;
    use LoliBlog::Controller::Posts;

    sub startup {
	my ($self) = @_;

	my $appConfig = $self
	    ->plugin('Config' => { file => './config/LoliBlog.conf' });

	push @{$self->static->paths} => $appConfig->{'blogDir'};

	$self->helper(
	    posts => sub {
		my $posts = LoliBlog::Model::Post->new($appConfig),
	    });

	$self->loadRoutings($appConfig);
    }

    sub loadRoutings {
	my ($self, $appConfig) = @_;

	my $route = $self->routes;

	# get
	# all-posts
	map {
	    $route
		->get($_ => { toSkip => 0 })
		->to('posts#posts');
	} '/', '/posts/all/:toSkip';

	# separate post
	$route
	    ->get('/post/show/:postId')
	    ->to('posts#showPost');

	# postForm
	$route
	    ->get('/posts/add')
	    ->to('posts#postForm');

		# postForm
	$route
	    ->post('/posts/add')
	    ->to('posts#addPost');

	# statics from post
	$route
	    ->get('/blog/static/*postEntity')
	    ->to('posts#postStaticEntity');
    }
}
1;
