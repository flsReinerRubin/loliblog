# Loli Blog #
## how to run ##

1. install http://mojolicio.us/
2. make posts dir $ mkdir -p $(loliBlogRoot)/blog/pub/
3. create post ../pub $ mkdir "V$(date +%Y%m%d%H%M%S)__custom_description"
4. write post "V$(date +%Y%m%d%H%M%S)__custom_description" $ vim pub # (first string is title, others are body (u can use html))
5. put any pictures (png|gif|jpeg|jpg)
6. $(loliBlogRoot) $ morbo LoliBlog.pl
7. Yay! 127.0.0.1:3000

### dir example ###
```
$(loliBlogRoot)
└── blog
    └── pub
        ├── V20141225144255__first_pub
        │   ├── 3931_993b.gif
        │   └── pub
        ├── V20141225180612__second_pub
        │   ├── 2440_3ee4.jpeg
        │   └── pub
        └── V20141225214203__thrid_pub
            ├── Capture.PNG
            └── pub
```

## add post ##
Now we (lol just me and my invisible friends) can add new post via http! Just click by "yay" in title (because I can't in UI).

Be careful because now it's impossible delete post.

## sass ##
### yeah! now we have got sass and compass conf ###
about compass and sass: [http://sass-lang.com/, http://compass-style.org/]
```bash
$ compass watch -c config/compassConfig.rb --app-dir $(pwd)
```